package kelkar.suraj;


import org.apache.pdfbox.pdfparser.PDFParser;
import org.apache.pdfbox.util.PDFTextStripper;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.net.URL;
import java.util.Set;

/**
 * Created by skelkar on 4/20/16.
 */
public class PDFReader {

    static String url = "http://qahome.mp.morningstar.com/home.asp";
    static WebDriver driver = new FirefoxDriver();

    public static void main(String[] args) throws InterruptedException, IOException {

            driver.get(url);
        String mainHandle = driver.getWindowHandle();
            driver.findElement(By.xpath("html/body/table[1]/tbody/tr[2]/td[3]/a/img")).click();
            Thread.sleep(5000);

            driver.findElement(By.xpath(".//*[@id='email']")).sendKeys("Demo2@foo.com");
            driver.findElement(By.xpath(".//*[@id='strPassword']")).sendKeys("_seR97%asd");
            driver.findElement(By.xpath(".//*[@id='btnLogin']")).click();
            Thread.sleep(15000);
            driver.findElement(By.xpath("html/body/div[2]/table/tbody/tr/td[13]/a/img")).click();
            driver.findElement(By.xpath("html/body/div[3]/table/tbody/tr[2]/td[1]/table/tbody/tr[4]/td[2]/div/table/tbody/tr[3]/td[2]")).click();

        waitForNewWindowAndSwitchToIt(driver);
        String newTitle = getCurrentWindowTitle();


        Thread.sleep(5000);
        URL url = new URL(driver.getCurrentUrl());
        System.out.print(url);
        BufferedInputStream fileToParse = new BufferedInputStream(url.openStream());

        PDFParser parser = new PDFParser(fileToParse);

        parser.parse();

        String output = new PDFTextStripper().getText(parser.getPDDocument());
       // System.out.println(output);
        Assert.assertTrue(output.contains("Inside Our Investment Process"));

        parser.getPDDocument().close();



        }


    public static String getCurrentWindowTitle() {
        String windowTitle = driver.getTitle();
        return windowTitle;
    }

    public static void waitForNewWindowAndSwitchToIt(WebDriver driver) throws InterruptedException {
        String cHandle = driver.getWindowHandle();
        String newWindowHandle = null;
        Set<String> allWindowHandles = driver.getWindowHandles();


            if (allWindowHandles.size() > 1) {
                for (String allHandlers : allWindowHandles) {
                    if (!allHandlers.equals(cHandle))
                        newWindowHandle = allHandlers;
                }
                driver.switchTo().window(newWindowHandle);

            }
        }

    }



