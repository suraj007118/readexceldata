package com.morningstar.test;

import com.morningstar.pages.LoginPage;
import com.morningstar.pages.TestBase;
import org.testng.annotations.Test;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;

public class SampleTest extends TestBase {

	@Test
	public void cancelitinerary() throws IOException, ParserConfigurationException, SAXException {
		
		setup();		
		LoginPage lp = new LoginPage(driver);
		lp.setusername(prop.getProperty("username"));
		lp.setpassword(prop.getProperty("password"));
		lp.clickloginbtn();


		driver.quit();
		

	}
}
