package com.morningstar.test;

import com.morningstar.pages.LoginPage;
import com.morningstar.pages.SearchHotelsPage;
import com.morningstar.pages.TestBase;
import com.morningstar.utility.GetExcelDataNew;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.util.HashMap;

public class SampleTest3 extends TestBase {

	@Test(dataProvider="Loginpagedata")
	public void LoginApp(HashMap<String, String> e) throws IOException, ParserConfigurationException, SAXException {
      //  WebDriver driver = new FirefoxDriver();
		setup();
        LoginPage lp = new LoginPage(driver);

        lp.setpassword(e.get("password"));
         lp.setusername(e.get("username"));


		lp.clickloginbtn();


		SearchHotelsPage sHP = new SearchHotelsPage(driver);
       sHP.setHotelLocation(e.get("HotelLocation"));
        sHP.searchhotelbtn().click();
		/*BookedIteneraryPage bIP = new BookedIteneraryPage(driver);
		sHP.BookedItinerarylink().click();
		bIP.desiredItenerarycheckbox().click();
		bIP.cancelItenerarybtn().click();*/
		driver.quit();
	}
	


    @DataProvider(name="Loginpagedata")
	public Object[][] getLoginPageData() throws IOException {
        GetExcelDataNew ex = new GetExcelDataNew("Loginpage");
        ex.open();
		 Object[][] arrayobject;
		arrayobject = ex.getData();
        ex.close();
        return arrayobject;


}


	//@AfterTest
	//public void teardown() throws IOException, SAXException, ParserConfigurationException, InterruptedException {
	//	new ExcelReportGenerator().generateExcelReport();
	//}
}
