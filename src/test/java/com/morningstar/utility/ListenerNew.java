package com.morningstar.utility;

/**
 * Created by skelkar on 4/11/16.
 */

import org.apache.commons.io.FileUtils;
import org.testng.*;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class ListenerNew implements ITestListener, ISuiteListener, IInvokedMethodListener {

    // This belongs to ISuiteListener and will execute before the Suite start
   // ReportExcel RExcel = new ReportExcel();
    List<Report> reports = new ArrayList<>();
    Report report;

    @Override

    public void onStart(ISuite arg0) {

        Reporter.log("About to begin executing Suite " + arg0.getName(), true);
        File reportdirectory = new File("./Reports");
        try {
            FileUtils.cleanDirectory(reportdirectory);
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    // This belongs to ISuiteListener and will execute, once the Suite is finished

    @Override

    public void onFinish(ISuite arg0) {
        try {
            new ReportExcel().printtoExcel(arg0, reports);
        } catch (IOException e) {
            e.printStackTrace();
        }
        Reporter.log("About to end executing Suite " + arg0.getName(), true);

    }

    // This belongs to ITestListener and will execute before starting of Test set/batch

    public void onStart(ITestContext arg0) {

        Reporter.log("About to begin executing Test " + arg0.getName(), true);

    }

    // This belongs to ITestListener and will execute, once the Test set/batch is finished

    public void onFinish(ITestContext arg0) {

        Reporter.log("Completed executing test " + arg0.getName(), true);

    }

    // This belongs to ITestListener and will execute only when the test is pass

    public void onTestSuccess(ITestResult arg0) {

        // This is calling the printTestResults method

        report = new Report();
        report.setStatus("Pass");


        reports.add(report);

    }


    // This belongs to ITestListener and will execute only on the event of fail test

    public void onTestFailure(ITestResult arg0) {

        // This is calling the printTestResults method

        report = new Report();
        report.setStatus("Failed");

        String exp_message = arg0.getThrowable().getMessage();
        String full_exp_message[] = exp_message.split("\n");
        String short_exp_message = full_exp_message[0];
        Reporter.log("Exception message=" + short_exp_message, true);
        report.setErrorMessage(short_exp_message);


        reports.add(report);
    }

    // This belongs to ITestListener and will execute before the main test start (@Test)

    public void onTestStart(ITestResult arg0) {

        System.out.println("The execution of the main test starts now");

    }

    // This belongs to ITestListener and will execute only if any of the main test(@Test) get skipped

    public void onTestSkipped(ITestResult arg0) {


    }

    // This is just a piece of shit, ignore this

    public void onTestFailedButWithinSuccessPercentage(ITestResult arg0) {

    }

    // This is the method which will be executed in case of test pass or fail

    // This will provide the information on the test


    // This belongs to IInvokedMethodListener and will execute before every method including @Before @After @Test

    public void beforeInvocation(IInvokedMethod arg0, ITestResult arg1) {

        String textMsg = "About to begin executing following method : " + returnMethodName(arg0.getTestMethod());

        Reporter.log(textMsg, true);

    }

    // This belongs to IInvokedMethodListener and will execute after every method including @Before @After @Test

    public void afterInvocation(IInvokedMethod arg0, ITestResult arg1) {

        String textMsg = "Completed executing following method : " + returnMethodName(arg0.getTestMethod());

        Reporter.log(textMsg, true);

    }

    // This will return method names to the calling function

    private String returnMethodName(ITestNGMethod method) {

        return method.getRealClass().getSimpleName() + "." + method.getMethodName();

    }

}
