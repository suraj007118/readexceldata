package com.morningstar.utility;

import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.testng.ISuite;
import org.testng.Reporter;

import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;

/**
 * Created by skelkar on 4/12/16.
 */
public class ReportExcel {

    static int r;

    public void printtoExcel(ISuite result, List<Report> reports) throws IOException {


        try {
        //File reportdirectory = new File("./Reports");

            //FileUtils.cleanDirectory(reportdirectory);

        String reportfilepath = "./Reports/report.xls";
        Workbook book = new XSSFWorkbook();
            Sheet sheet = book.createSheet("Result");

        CellStyle fail = book.createCellStyle();
        CellStyle pass = book.createCellStyle();
        CellStyle skip = book.createCellStyle();

        String test_name = result.getXmlSuite().toString();
        Reporter.log("Test name: " + test_name, true);
            for (Report report: reports
                 ) {
                String status = report.getStatus();
                String class_name = result.getXmlSuite().getName();
                Reporter.log("class name: " + class_name, true);
                String test_method_name = result.getName().toString();
                Reporter.log("Test method name " + test_method_name, true);
                Reporter.log("Test Status: " + status, true);
                fail.setFillForegroundColor(HSSFColor.RED.index);
                pass.setFillForegroundColor(HSSFColor.BRIGHT_GREEN.index);
                skip.setFillForegroundColor(HSSFColor.BROWN.index);
                fail.setFillPattern(XSSFCellStyle.SOLID_FOREGROUND);
                pass.setFillPattern(XSSFCellStyle.SOLID_FOREGROUND);
                skip.setFillPattern(XSSFCellStyle.SOLID_FOREGROUND);

                //Excel file
                Row row = sheet.createRow(r++);
                Cell cel_name = row.createCell(0);
                cel_name.setCellValue(class_name + "." + test_method_name);
                Cell cel_status = row.createCell(1);
                cel_status.setCellValue(status);
                Cell cel_expMessage = row.createCell(2);
                cel_expMessage.setCellValue(report.getErrorMessage());

                switch (status){

                    case "Failed":
                        cel_status.setCellStyle(fail);
                        break;

                    case "Pass":
                        cel_status.setCellStyle(pass);
                        break;
                    case "Skipped":
                        cel_status.setCellStyle(skip);

          }

            }



        FileOutputStream fout = new FileOutputStream(reportfilepath);

            book.write(fout);

        fout.close();
        System.out.println("Report Generated");
        } catch (IOException e) {
            e.printStackTrace();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }


}
