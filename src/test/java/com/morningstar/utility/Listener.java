package com.morningstar.utility;

/**
 * Created by skelkar on 4/11/16.
 */

        import org.apache.commons.io.FileUtils;
import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.testng.*;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

public class Listener implements ITestListener, ISuiteListener, IInvokedMethodListener {

    // This belongs to ISuiteListener and will execute before the Suite start

    @Override

    public void onStart(ISuite arg0) {

        Reporter.log("About to begin executing Suite " + arg0.getName(), true);

    }

    // This belongs to ISuiteListener and will execute, once the Suite is finished

    @Override

    public void onFinish(ISuite arg0) {

        Reporter.log("About to end executing Suite " + arg0.getName(), true);

    }

    // This belongs to ITestListener and will execute before starting of Test set/batch

    public void onStart(ITestContext arg0) {

        Reporter.log("About to begin executing Test " + arg0.getName(), true);

    }

    // This belongs to ITestListener and will execute, once the Test set/batch is finished

    public void onFinish(ITestContext arg0) {

        Reporter.log("Completed executing test " + arg0.getName(), true);

    }

    // This belongs to ITestListener and will execute only when the test is pass

    public void onTestSuccess(ITestResult arg0) {

        // This is calling the printTestResults method

        printTestResults(arg0);
        try {
            printtoExcel(arg0);
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    // This belongs to ITestListener and will execute only on the event of fail test

    public void onTestFailure(ITestResult arg0) {

        // This is calling the printTestResults method


        printTestResults(arg0);
        try {
            printtoExcel(arg0);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    // This belongs to ITestListener and will execute before the main test start (@Test)

    public void onTestStart(ITestResult arg0) {

        System.out.println("The execution of the main test starts now");

    }

    // This belongs to ITestListener and will execute only if any of the main test(@Test) get skipped

    public void onTestSkipped(ITestResult arg0) {

        printTestResults(arg0);

    }

    // This is just a piece of shit, ignore this

    public void onTestFailedButWithinSuccessPercentage(ITestResult arg0) {

    }

    // This is the method which will be executed in case of test pass or fail

    // This will provide the information on the test

    public void printtoExcel(ITestResult result) throws IOException {
        int r=0;
        File reportdirectory = new File("./Reports");
        try {
            FileUtils.cleanDirectory(reportdirectory);
        } catch (IOException e) {
            e.printStackTrace();
        }
        String reportfilepath = "./Reports/report.xls";
        Workbook book = new XSSFWorkbook();
        CellStyle fail = book.createCellStyle();
        CellStyle pass = book.createCellStyle();
        CellStyle skip = book.createCellStyle();

        String test_name = result.getMethod().toString();
        Reporter.log("Test name: " + test_name, true);
        Sheet sheet = book.createSheet(test_name);
        String class_name = result.getTestClass().getName();
        Reporter.log("class name: " + class_name, true);
        String test_method_name = result.getName().toString();
        Reporter.log("Test method name " + test_method_name, true);
        String exp_message = result.getThrowable().getMessage();
        String full_exp_message []= exp_message.split("\n");
        String short_exp_message = full_exp_message[0];
        Reporter.log("Exception message="+short_exp_message,true);




        String status = null;

        switch (result.getStatus()) {

            case ITestResult.SUCCESS:

                status = "Pass";

                break;

            case ITestResult.FAILURE:

                status = "Failed";

                break;

            case ITestResult.SKIP:

                status = "Skipped";

        }

        Reporter.log("Test Status: " + status, true);

        fail.setFillForegroundColor(HSSFColor.RED.index);
        pass.setFillForegroundColor(HSSFColor.BRIGHT_GREEN.index);
        skip.setFillForegroundColor(HSSFColor.BROWN.index);

        fail.setFillPattern(XSSFCellStyle.SOLID_FOREGROUND);
        pass.setFillPattern(XSSFCellStyle.SOLID_FOREGROUND);
        skip.setFillPattern(XSSFCellStyle.SOLID_FOREGROUND);



        //Excel file
        Row row = sheet.createRow(r++);
        Cell cel_name = row.createCell(0);
        cel_name.setCellValue(class_name + "." + test_method_name);
        Cell cel_status = row.createCell(1);
        cel_status.setCellValue(status);
        Cell cel_expMessage = row.createCell(2);
        cel_expMessage.setCellValue(short_exp_message);

        switch (status){

            case "Failed":
                cel_status.setCellStyle(fail);
                break;

            case "Pass":
                cel_status.setCellStyle(pass);
                break;
            case "Skipped":
            cel_status.setCellStyle(skip);

      /*  if("Failed".equalsIgnoreCase(status))
            cel_status.setCellStyle(fail);
        else
            cel_status.setCellStyle(pass);
        */
        }
        FileOutputStream fout = null;
        try {
            fout = new FileOutputStream(reportfilepath);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        try {
            book.write(fout);
        } catch (IOException e) {
            e.printStackTrace();
        }
        fout.close();
        System.out.println("Report Generated");
    }










    public void printTestResults(ITestResult result) {

        Reporter.log("Test Method resides in " + result.getTestClass().getName(), true);

        if (result.getParameters().length != 0) {

            String params = null;

            for (Object parameter : result.getParameters()) {

                params += parameter.toString() + ",";

            }

            Reporter.log("Test Method had the following parameters : " + params, true);

        }

        String status = null;

        switch (result.getStatus()) {

            case ITestResult.SUCCESS:

                status = "Pass";

                break;

            case ITestResult.FAILURE:

                status = "Failed";

                break;

            case ITestResult.SKIP:

                status = "Skipped";

        }

        Reporter.log("Test Status: " + status, true);

    }

    // This belongs to IInvokedMethodListener and will execute before every method including @Before @After @Test

    public void beforeInvocation(IInvokedMethod arg0, ITestResult arg1) {

        String textMsg = "About to begin executing following method : " + returnMethodName(arg0.getTestMethod());

        Reporter.log(textMsg, true);

    }

    // This belongs to IInvokedMethodListener and will execute after every method including @Before @After @Test

    public void afterInvocation(IInvokedMethod arg0, ITestResult arg1) {

        String textMsg = "Completed executing following method : " + returnMethodName(arg0.getTestMethod());

        Reporter.log(textMsg, true);

    }

    // This will return method names to the calling function

    private String returnMethodName(ITestNGMethod method) {

        return method.getRealClass().getSimpleName() + "." + method.getMethodName();

    }

}
