package com.morningstar.utility;

import org.apache.commons.io.FileUtils;
import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
//import org.w3c.dom.Element;



public class ExcelReportGenerator {

	public void generateExcelReport() throws ParserConfigurationException, SAXException, IOException, InterruptedException {

		String path = ExcelReportGenerator.class.getClassLoader().getResource("./").getPath();
		System.out.println(path);
		path = path.replaceAll("bin", "src");
		System.out.println(path);

		File xmlFile = new File("./target/surefire-reports/testng-results.xml");

        File reportdirectory = new File("./Reports");
        FileUtils.cleanDirectory(reportdirectory);
       Thread.sleep(5);
        String reportfilepath = "./Reports/report.xls";
        DocumentBuilderFactory fact = DocumentBuilderFactory.newInstance();
		DocumentBuilder build = fact.newDocumentBuilder();
		Document doc = build.parse(xmlFile);
		doc.getDocumentElement().normalize();
		Workbook book = new XSSFWorkbook();
		CellStyle fail = book.createCellStyle();
		CellStyle pass = book.createCellStyle();

		NodeList test_list = (NodeList) doc.getElementsByTagName("test");


		for(int i =0; i<test_list.getLength();i++){
			int r=0;
			org.w3c.dom.Node test_node = test_list.item(i);
			String test_name = ((org.w3c.dom.Element)test_node).getAttribute("name");
			Sheet sheet = book.createSheet(test_name);
			NodeList class_list = ((org.w3c.dom.Element)test_node).getElementsByTagName("class");

			for(int j=0;j< class_list.getLength();j++){
				org.w3c.dom.Node class_node = class_list.item(j);
				String class_name = ((org.w3c.dom.Element)class_node).getAttribute("name");

				NodeList test_method_list = ((org.w3c.dom.Element)class_node).getElementsByTagName("test-method");

				for(int k=0;k<test_method_list.getLength();k++){
					org.w3c.dom.Node test_method_node = test_method_list.item(k);
					String test_method_name = ((org.w3c.dom.Element)test_method_node).getAttribute("name");
					String test_method_status = ((org.w3c.dom.Element)test_method_node).getAttribute("status");
					//k=k+2;


					fail.setFillForegroundColor(HSSFColor.RED.index);
					pass.setFillForegroundColor(HSSFColor.BRIGHT_GREEN.index);

					fail.setFillPattern(XSSFCellStyle.SOLID_FOREGROUND);
					pass.setFillPattern(XSSFCellStyle.SOLID_FOREGROUND);			


					//Excel file
					Row row = sheet.createRow(r++);
					Cell cel_name = row.createCell(0);
					cel_name.setCellValue(class_name + "." + test_method_name);
                    Cell cel_status = row.createCell(1);
					cel_status.setCellValue(test_method_status);

					if("FAIL".equalsIgnoreCase(test_method_status))
						cel_status.setCellStyle(fail);
					else
						cel_status.setCellStyle(pass);	

					Cell cel_exp;
					String exp_msg;

					if("FAIL".equalsIgnoreCase(test_method_status)){
						NodeList exp_list = ((org.w3c.dom.Element)test_method_node).getElementsByTagName("exception");

						System.out.println(exp_list);						
						org.w3c.dom.Node exp_node = exp_list.item(0);
						System.out.println(exp_node);
						exp_msg = ((org.w3c.dom.Element)exp_node).getAttribute("class");
						System.out.println(exp_msg);


						cel_exp = row.createCell(2);
						cel_exp.setCellValue(exp_msg);
					}
				}
			}
		}

		FileOutputStream fout = new FileOutputStream(reportfilepath);
		book.write(fout);
		fout.close();
		System.out.println("Report Generated");
	}



}
