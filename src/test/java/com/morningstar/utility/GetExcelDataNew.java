package com.morningstar.utility;

/**
 * Created by skelkar on 4/6/16.
 */
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.HashMap;
//  import org.dom4j.DocumentException;


public class GetExcelDataNew {
        int rowIndex = 0, columnIndex = 0;
                Workbook wb;
                Sheet ws;
                Row wr;
                String fileName;
                String sheetName;
                HashMap<String, String>[] data = null;

        public GetExcelDataNew(String sheetName) {
                this.sheetName = sheetName;
        }

        public  void open() throws IOException  {

    String fileName = "./TestData/testData.xlsx";
        File file = new File(fileName);
    FileInputStream fs = new FileInputStream(file);
    wb = new XSSFWorkbook(fs);
        ws = wb.getSheet(sheetName);
        }


@SuppressWarnings("unchecked")
public Object[][] getData() throws IOException  {
        data = new HashMap[ws.getPhysicalNumberOfRows()];
        wr = ws.getRow(0);

        for(rowIndex = 1; rowIndex < ws.getPhysicalNumberOfRows(); rowIndex++) {

        data[rowIndex - 1] = new HashMap<String, String>();

        for (columnIndex = 0; columnIndex < ws.getRow(rowIndex).getPhysicalNumberOfCells(); columnIndex++) {
        data[rowIndex - 1].put(wr.getCell(columnIndex).toString(), ws.getRow(rowIndex).getCell(columnIndex).toString());
        }
        }

        Object[][] obj = new Object[data.length - 1][1];
        for(int i = 0; i < data.length - 1; i++) {
        obj[i][0] = data[i];
        }
        return obj;
        }


public  void close() throws IOException  {
        wb = null;
        ws = null;
        wr = null;
        data = null;
        }


        }

