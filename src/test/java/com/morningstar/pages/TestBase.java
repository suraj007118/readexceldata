package com.morningstar.pages;

import com.morningstar.cons.Constants;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

import java.io.IOException;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

public class TestBase {

	 public Properties prop=new Properties();
	public static WebDriver driver = null;

	public void setup() throws IOException {
		driver = new FirefoxDriver();
		driver.get(Constants.BASE_URL);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	}
}
