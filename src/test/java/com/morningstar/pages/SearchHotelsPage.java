package com.morningstar.pages;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

public class SearchHotelsPage {

	
	
	private WebDriver driver;


	public SearchHotelsPage(WebDriver driver) {
		this.driver = driver;
	}
	public WebElement element = null;
	private static final Logger log = Logger.getLogger(SearchHotelsPage.class);
	
	
	public WebElement hotellocation () {
		element = driver.findElement(By.cssSelector("select.search_combobox[name=location]"));
		return element;	
	}
	
	public WebElement adultperroom () {
		element = driver.findElement(By.cssSelector("select[name=adult_room]"));
		return element;	
	}

	public WebElement searchhotelbtn () {
		element = driver.findElement(By.cssSelector("input.reg_button"));
		return element;	
	}
	
	public WebElement BookedItinerarylink() {
		element = driver.findElement(By.linkText("Booked Itinerary"));
		return element;
}
	public WebElement logoutlink () {
		element =  driver.findElement(By.linkText("Logout"));
		return element;
}

	public void setHotelLocation(String HotelLocation){
		Select hotellocation = new Select(hotellocation());
		hotellocation.selectByValue(HotelLocation);
	}

}